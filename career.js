//Search Area
const searchedJob = document.forms['careerSearchForm'];
careerSearchForm.addEventListener('submit', searchFunctiona, false);
let titleInput, locationInput, typeInput;
 
function searchFunctiona(event) {
  event.preventDefault();
  let searchData = {};
  searchData.title = searchedJob.jobTitle.value;
  searchData.location = searchedJob.jobLocation.value;
  searchData.type = searchedJob.jobType.value;
  searchFunction(searchData)
  return searchData
}

readFromLocalStorage()

let mainSearchCareer = document.forms['careerSearchForm'];

// mainSearchCareer.addEventListener('submit', searchJobs)

function searchJobs (e){
e.preventDefault();
    
const searchAreaTitle = document.querySelector('#searchArea').value.trim();
const searchAreaIndustry = document.querySelector('#searchArea1').value.trim();
const searchAreaLocation = document.querySelector('#searchArea11').value.trim();

const filteredJobs = [];
if( (searchAreaTitle || searchAreaTitle != '') && (searchAreaIndustry || searchAreaIndustry != '') && (searchAreaLocation || searchAreaLocation != '')  ){
const jobData = readFromLocalStorage();
if(jobData && jobData.length > 0){
for( let i = 0 ; i < jobData.length ; i++ ){
let currentJob = jobData[i];


if(currentJob.jobLocation.toLowerCase().includes(searchAreaLocation .toLowerCase()) && currentJob.jobType.toLowerCase().includes(searchAreaIndustry.toLowerCase()) && currentJob.jobTitle.toLowerCase().includes(searchAreaTitle.toLowerCase())){
    filteredJobs.push(currentJob); 
  }else{
    continue;
  }
  }
  console.log(filteredJobs);
  }else{
  console.log('There are no jobs available');
  }
  }else{
  console.log('invalid input');
    }
}

function readFromLocalStorage(){
    let dataStore = localStorage.getItem('adminData');
    if(dataStore){ // check if data exists
      return JSON.parse(dataStore);
    }else{
      return false;
    }
  }






// let formH = document.forms['loginForm'];

// formH.addEventListener('submit', loginFunction )


// function loginFunction(event){
//     event.preventDefault();
//     const loginData = {}
//     loginData.email =formH.email.value;   
//     loginData.password =formH.password.value;
   
//     console.log(loginData);
// }



function searchFunction(search) {
  const { location, type, title } = search;
  let allsearch = location.trim().toLowerCase() || type.trim().toLowerCase() || title.trim().toLowerCase();
  let data = JSON.parse(localStorage.getItem('jobs'));
 
  let filteredData = [];
  if (allsearch.length > 0) {
    filteredData = data.filter(function (obj) {
      return Object.keys(obj).some(function (key) {
        return obj[key].toLowerCase().includes(allsearch);
      });
    });
  }
  renderToIndex(filteredData);
  //getJobCreated2(filteredData)
  // return data;
}

function renderToIndex (jobs){
  // console.log(jobs);
  let list = '';

  if(Array.isArray(jobs)){
   
    for( let i = 0 ; i < jobs.length ; i++ ){
    let currentJob = jobs[i];

    // let currentJob = postedData;
    
    list += "<li class='singleList'><p class='TitleHead font-weight-bolder'>Title</p> <p class='jobTitle'>" + currentJob['jobTitle'] + "</p> <p class='locationHead'>Location</p> <p class='jobLocation'>" + currentJob['jobLocation'] + " </p> <p class='typeHead'> Type</p> <p class='Type'>" + currentJob['jobType'] ;
    list += "</p> <p class= 'descriptionHead'>Description</p> <p class= 'Description'>" + currentJob['jobDescription'] + "</p> <p class= 'qualificationHead'>Qualification</p> <p class= 'qualification'>" + currentJob['qualification'] + "</p><p class= 'datePosted'>Date Posted</p><p class= 'datePosted'>" +  currentJob['datePosted'] + "</p> <p class= 'dateClosed'>Date Closed</p> <p class= 'dateClosed'>" + currentJob['dateClosed'] + "</p></li>" ; 
    
    }  
    document.querySelector('#jobLists').innerHTML = list;
    }
    // alert('error!! incorrect data in local storage');
}

renderToIndex (JSON.parse(localStorage.getItem("jobs")));
// document.querySelector('#jobLists').addEventListener('click', renderToIndex);

//form validation

// const jobform = document.forms['loginForm']
// signUp.addEventListener('submit',(event)=>{
//   event.preventDefault();

// function adminValidation() {
//   var loginUsername = document.getElementById('Username');
//   var loginPassword = document.getElementById('password');
// let username = 'immaculate.egwu@venturegardengroup.com';
// if(loginUsername.value == 'immaculate.egwu@venturegardengroup.com');{
//   window.location.href = "admin.html";
// }
// else if(loginUsername.value == '' || loginPassword.value == '') {
// alert('No bank space allowed');  
// } 
// else {
  
// }
//  else{
//   window.location.href = "client.html"; 
//  }
// }
// }

// document.querySelector('#loginForm').addEventListener('input', (event) => {
// console.log(event.target.value);
// }
// )

let arrS = JSON.parse(localStorage.getItem('jobs'));
uniquetitle(arrS, 'jobTitle')

function uniquetitle(arr, comp) {

  if ((localStorage.getItem('jobs') !== null)) {
 
    const uniqueTitle = arr
      .map(e => e[comp])
 
      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)
 
      // eliminate the dead keys & store unique objects
      .filter(e => arr[e]).map(e => arr[e]);
      filterTitle(uniqueTitle)
  } else return
}

function filterTitle(unique) {
  const selectElement = ` 
        <select class="form-control" name="jobTitle" id="jobTitle" required>
        <option disabled selected value="">Please select Job Title</option>
         ${unique.map(i => `<option>${i.jobTitle.toLowerCase().split(' ').map(w => w.substring(0, 1).toUpperCase() + w.substring(1)).join(' ')}</option>`)}
         </select>`;
  document.getElementById('jobTitleSelect').innerHTML = selectElement;
}



uniquejobLocationSelect(arrS, 'jobLocation')

function uniquejobLocationSelect(arr, comp) {

  if ((localStorage.getItem('jobs') !== null)) {
 
    const uniqueLocation = arr
      .map(e => e[comp])
 
      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)
 
      // eliminate the dead keys & store unique objects
      .filter(e => arr[e]).map(e => arr[e]);
      filterjobLocationSelect(uniqueLocation)
  } else return
}

function filterjobLocationSelect(uniquen) {
  const selectElement = ` 
        <select class="form-control" name="jobLocation" id="jobLocation" required>
        <option disabled selected value="">Please select Job Location</option>
         ${uniquen.map(i => `<option>${i.jobLocation.toLowerCase().split(' ').map(w => w.substring(0, 1).toUpperCase() + w.substring(1)).join(' ')}</option>`)}
         </select>`;
  document.getElementById('jobLocationSelect').innerHTML = selectElement;
}


uniquejobjobTypeSelect(arrS, 'jobType')

function uniquejobjobTypeSelect(arr, comp) {

  if ((localStorage.getItem('jobs') !== null)) {
 
    const uniquejobType = arr
      .map(e => e[comp])
 
      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)
 
      // eliminate the dead keys & store unique objects
      .filter(e => arr[e]).map(e => arr[e]);
      filterjobjobTypeSelect(uniquejobType)
  } else return
}

function filterjobjobTypeSelect(uniquen) {
  const selectElement = ` 
        <select class="form-control" name="jobType" id="jobType" required>
        <option disabled selected value="">Please select Job Type</option>
         ${uniquen.map(i => `<option>${i.jobType.toLowerCase().split(' ').map(w => w.substring(0, 1).toUpperCase() + w.substring(1)).join(' ')}</option>`)}
         </select>`;
  document.getElementById('jobTypeSelect').innerHTML = selectElement;
}

// input field
const input = document.querySelector('input[type="file"]')
input.addEventListener('change', function (e) {
  console.log(input.files)
  const reader = new FileReader()
  reader.onload = function(){
    // console.log(reader.result)
  
  const lines = reader.result.split('\n').map(function (line){
    return line.split(',')
  })
  console.log(lines)

  // ...setting Image...comment reader.readAsTest and const lines that involves mapping then the below function
  // const img = new Image()
  // img.src = reader.result
  // document.body.appendChild(img)
}
      // call back
      reader.readAsText(input.files[0])
  
      reader.readAsDataURL(input.files[0])
 
},false)
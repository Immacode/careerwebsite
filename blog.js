const allPost = document.getElementById('posts');
const comments = document.getElementById('comments');
const url ='http://jsonplaceholder.typicode.com/posts';

function blog(){
    fetch(url)
    .then(response => response.json())
    .then(data => {
      // console.log(data) 

      (function(){

        document.getElementById("first").addEventListener("click", firstPage);
        document.getElementById("next").addEventListener("click", nextPage);
        document
          .getElementById("previous")
          .addEventListener("click", previousPage);
        document.getElementById("last").addEventListener("click", lastPage);
      
      var list = data
      var pageList = new Array();
      var currentPage = 1;
      var numberPerPage = 10;
      var numberOfPages = 0;

      function makeList() {
      numberOfPages = getNumberOfPages();
      }
      
      function getNumberOfPages() {
      return Math.ceil(list.length / numberPerPage);
      }
      function nextPage() {
      currentPage += 1;
      loadList();
      }
      function previousPage() {
      currentPage -= 1;
      loadList();
      }
      function firstPage() {
      currentPage = 1;
      loadList();
      }
      function lastPage() {
      currentPage = numberOfPages;
      loadList();
      }
      function loadList() {
      var begin = ((currentPage - 1) * numberPerPage);
      var end = begin + numberPerPage;
      pageList = list.slice(begin, end);
      drawList();
      check();
      }
      
      function drawList() {
        allPost.innerHTML = "";
        pageList.forEach((post)=>{
          allPost.innerHTML +=  
          `<div class ="postDisplay">
          <img src="images/customer.jpg"><br>
          <h3 class="titleAdded"> Happy customers week to all our customers</h3>
          <h5 class="title"> ${post.title} </h5>
          <h6 class="body">" ${post.body}</h6>
          <a href='onepost.html?id=${post.id}' >View Post</a></div>`
        })
      }

      function check() {
      document.getElementById("next").disabled = currentPage == numberOfPages ? true : false;
      document.getElementById("previous").disabled = currentPage == 1 ? true : false;
      document.getElementById("first").disabled = currentPage == 1 ? true : false;
      document.getElementById("last").disabled = currentPage == numberOfPages ? true : false;
      }

      function load() {
      makeList();
      loadList();
      }
      
      window.onload = load();
      })();
    })
    // .catch(error => console.error(error));
}

blog();


function getUrlParameter(name) {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  var results = regex.exec(location.search);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};
let id = getUrlParameter('id');
console.log(id);
var commentsUrl =`http://jsonplaceholder.typicode.com/comments?postId=${id}`;

function commentsFunc(){
  fetch(commentsUrl)
  .then(response => response.json())
  .then(data => {
    console.log(data) 
    // arrayData(data);
    
    data.forEach((post)=>{
      comments.innerHTML +=  
      
      `<div class ="postDisplay">
      <img src="images/customer.jpg"><br>
      <h3class='titleAdded> Happy customers week to all our customers</h3class='titleAdded>
      <h5 class='title'> ${post.name} </h5>
      <h6 class='body'>" ${post.body}</h6>
      <h6 class='body'>" ${post.email}</h6>
      </div>`

    })
  })
  .catch(error => console.error(error));
}
commentsFunc();